//!user bo'limi uchun yozilgan js :
const product = [
    {
        id: 1,
        image: "./img/img1.png",
        title: "t-shirt",
        price: 110
    },
    {
        id: 1,
        image: "./img/img2.png",
        title: "t-shirt",
        price: 90,
    },
    {
        id: 1,
        image: "./img/img3.png",
        title: "t-shirt",
        price: 80
    },
    {
        id: 1,
        image: "./img/img4.png",
        title: "t-shirt2506",
        price: 50
    },
]

const categories = [...new Set(product.map(item => item))]

let i = 0;
function usersProduct(){
    document.getElementById("root").innerHTML = product.map(item => {
        var { image, title, price, } = item;
        return `<div class="box">
                <div className="image-box">
                    <img src=${image} alt="" />
                </div>
                <div className="bottom">
                    <p>${title}</p>
                    <h2>Price ${price}.00</h2>
                    <div class="button--like">
                    <button onClick="addToCart(${i})">Add to cart</button>
                    <i onClick="addToLike(${i++})" class="like">&#10084</i>
                    </div>
                    
                </div>
            </div>`}).join("")
}
document.addEventListener("DOMContentLoaded",usersProduct());
//*array for basket 
const cart = [];
//*delete iconi bosilganda aynan bosilgan cart o'chib ketishi uchun yozilgan funksiya
function delElement(id) {
    cart.splice(id, 1);
    //*bosilgan cart delete bo'lgandan so'ng qolgan cartlarni qaytadan chizib berish uchun displayCart funksiyasini chaqirdik
    displayCart();
}
//*cartga qo'shish funksiyasini yozdik
function addToCart(id) {
    cart.push(product[id])
    //*qo'shilgan cartlarni chizish uchun displayCart funksiyasini chaqirdik
    displayCart()
}
function totalRes() {
    let result = 0;
    cart.map(item => result += item.price);
    return result;
}
//*kelgan cartni sidebarga chizish uchun funksiya yozdik
function displayCart() {
    let j = 0;
    document.getElementById("count").textContent = cart.length;
    document.querySelector("#total").textContent = totalRes();
    if (cart.length == 0) {
        document.querySelector(".cartItem").textContent = "Savatingiz bo'sh";
    } else {
        document.querySelector(".cartItem").innerHTML = cart.map((item) => {
            //! forEach va mapning farqi :  map yangi array qaytaradi
            var { image, title, price, } = item;
            return `<div class="cart-item">
                        <div class="row-img">
                            <img class="rowing" src=${image} alt="" />
                        </div>
                        <p style="font-size:12px">${title}</p>
                        <h2 style="font-size: 15px">${price}.00</h2>
                        <i class="fa fa-trash" onclick='delElement(${j++})'></i>
                    </div>`;
        }).join("");

    }
}
//*like bosilgan cartlarni yig'ish uchun bo'sh massiv ochdik
const likeBasket = [];
//*like bosish tugmasi bosilganda quyidagi funksiya chaqiriladi va yuqoridagi i qiymati idga beriladi va aynan shu iddagi cart likeBasketga qo'shiladi
function addToLike(id) {
    likeBasket.push(categories[id]);
    //*yangi cart qo'shilgandan keyin displayLike funksiyasi hamma cartlarni qaytadan chizib beradi agar bu funksiyani chaqirmasak yangilangan cartlar htmlga chizilmaydi lekin korzinkadagi qiymat o'zgaraveradi
    displayLike();
}
//*delete buttoni bosilganda like bosilgan cartlar ichidagi aynan idga mos cart o'chib ketadi quyidagi funksiya shu vazifani bajaradi
function deleteLike(id) {
    likeBasket.splice(id, 1);
    displayLike();
}
//*o'zgartirilgan cartlarni qaytadan chizib berish uchun quyidagi funksiya yozildi
function displayLike() {
    document.querySelector("#likeCount").textContent = likeBasket.length;
    let j = 0;
    if (likeBasket.length == 0) {
        document.querySelector(".likeItem").textContent = "Hozircha yoqtirganlaringiz mavjud emas";
    } else {
        document.querySelector(".likeItem").innerHTML = likeBasket.map((item) => {
            var { image, title, price, } = item;
            return `<div class="cart-item">
                        <div class="row-img">
                            <img class="rowing" src=${image} alt="" />
                        </div>
                        <p style="font-size:12px">${title}</p>
                        <h2 style="font-size: 15px">${price}.00</h2>
                        <i class="fa fa-trash" onclick='deleteLike(${j++})'></i>
                    </div>`;
        }).join("");

    }
}
//!admin bo'limi uchun yozilgan js :
//     const adminCart = [];
// //*cartdagi delete buttoni bosilganda cart butunlay o'chib betishi uchun quyidagi funksiya yozildi
// function deletedCart(id) {
//     categories.splice(id, 1);
//     adminCartFunction();
//     usersProduct()
//     // product.splice(id,1);
// }
// //*o'chirilgan cartlarni bo'sh massivimizga qo'shish uchun quyidagi funksiya yozildi
// function addAdminCart(id) {
//     adminCart.push(categories[id]);
//     addDeletCart();
//     deletedCart(id);
// }
// function totalRes() {
//     let result = 0;
//     adminCart.map(item => result += item.price);
//     return result;
// }
// //*delete qilingan cartlarni sidebarga chizib berish uchun quyidagi funksiya yozildi 
// function addDeletCart() {
//     document.querySelector("#total").textContent = totalRes();
//     if (adminCart.length == 0) {
//         document.querySelector(".cartItem").textContent = "Birorta Cart o'chirilmagan"
//     } else {
//         document.querySelector(".cartItem").innerHTML = adminCart.map(item => {
//             var { image, title, price } = item;
//             return `<div class="cart-item">
//                         <div class="row-img">
//                             <img class="rowing" src=${image} alt="" />
//                         </div>
//                         <p style="font-size:12px">${title}</p>
//                         <h2 style="font-size: 15px">${price}.00</h2>
                        
//                     </div>`;
//         }).join("");
//     }
// }
// //*rootga cartlarni chizib beradi
// function adminCartFunction() {
//     let k = 0;
//     document.querySelector("#root").innerHTML = categories.map(item => {
//         var { image, title, price, id } = item;
//         return `<div class="box">
//                 <div className="image-box">
//                     <img src=${image} alt="" />
//                 </div>
//                 <div className="bottom">
//                     <p>${title}</p>
//                     <h2>Price ${price}.00</h2>
//                     <div class="button--like">
//                      <button onClick="editedToCart(${k})">Edited to cart</button>
//                     <button onClick="addAdminCart(${k++})">Deleted Cart</button>
//                     </div>  
//                 </div>
//             </div>`
//     }).join("");
// }

// document.addEventListener("DOMContentLoaded", adminCartFunction());
// const close = document.querySelector("#close");
// const modal = document.querySelector(".wrapper__Modal");
// const shadow = document.querySelector(".shadow");
// const title = document.getElementsByClassName("input-title");
// const price = document.getElementsByClassName("input-price");
// const editedButton = document.querySelector("#modal__button");
// function editedToCart() {
//     modal.classList.toggle("remove");
//     shadow.classList.toggle("remove");
// }
// close.addEventListener("click", () => {
//     modal.classList.toggle("remove");
//     shadow.classList.toggle("remove");
// })

// function valueInput(index) {
//     categories[index].title = title.value;
//     categories[index].price = price.value;
// }


// //*edit qilingan cartlarni sidebarga qo'shamiz
// const editCart = [];
// editedButton.addEventListener("click", (id) => {
//     valueInput(id);
//     editCart.push(categories[id]);
//     addEditedCart();
//     modal.classList.toggle("remove");
//     shadow.classList.toggle("remove");
// })
// function addEditedCart() {
//     let l = 0;
//     document.querySelector(".likeItem").innerHTML = editCart.map(item => {
//         var { image, title, price, id } = item;
//         return `<div class="box">
//                 <div className="image-box">
//                     <img src=${image} alt="" />
//                 </div>
//                 <div className="bottom">
//                     <p>${title}</p>
//                     <h2>Price ${price}.00</h2>
//                     <div class="button--like">
//                      <button onClick="editedToCart(${l})">Edited to cart</button>
//                     <button onClick="addAdminCart(${l++})">Deleted Cart</button>
//                     </div>  
//                 </div>
//             </div>`
//     }).join("");
// }