const product = [
    {
        id: 1,
        image: "./img/img1.png",
        title: "t-shirt",
        price: 110
    },
    {
        id: 1,
        image: "./img/img2.png",
        title: "t-shirt",
        price: 90,
    },
    {
        id: 1,
        image: "./img/img3.png",
        title: "t-shirt",
        price: 80
    },
    {
        id: 1,
        image: "./img/img4.png",
        title: "t-shirt2506",
        price: 50
    },
]
let categories = [...new Set(product.map(item => item))];

const adminCart = [];
//*cartdagi delete buttoni bosilganda cart butunlay o'chib betishi uchun quyidagi funksiya yozildi
function deletedCart(id) {
    categories.splice(id, 1);
    localStorage.setItem("state", JSON.stringify(categories))
    adminCartFunction();


}
//*o'chirilgan cartlarni bo'sh massivimizga qo'shish uchun quyidagi funksiya yozildi
function addAdminCart(id){
    adminCart.push(categories[id]);
    addDeletCart();
    deletedCart(id);
}
function totalRes() {
    let result = 0;
    adminCart.map(item => result += item.price);
    return result;
}
//*delete qilingan cartlarni sidebarga chizib berish uchun quyidagi funksiya yozildi 
function addDeletCart() {
    document.querySelector("#total").textContent=totalRes();
    if (adminCart.length == 0) {
        document.querySelector(".cartItem").textContent = "Birorta Cart o'chirilmagan"
    } else {
        document.querySelector(".cartItem").innerHTML = adminCart.map(item => {
            var { image, title, price} = item;
            return `<div class="cart-item">
                        <div class="row-img">
                            <img class="rowing" src=${image} alt="" />
                        </div>
                        <p style="font-size:12px">${title}</p>
                        <h2 style="font-size: 15px">${price}.00</h2>
                        
                    </div>`;
        }).join("");
    }
}
//*rootga cartlarni chizib beradi
// localStorage.setItem("state",JSON.stringify(categories));
// console.log(localStorage.getItem("state"));
// localStorage.clear();
function adminCartFunction() {
    if(localStorage.getItem("state")){
        categories = [...JSON.parse(localStorage.getItem("state"))]
        console.log("state bor ");
    }else{
        console.log(categories);
    }
    let k = 0;
    document.querySelector("#root").innerHTML = categories.map(item => {
        var { image, title, price,id} = item;
        return `<div class="box">
                <div className="image-box">
                    <img src=${image} alt="" />
                </div>
                <div className="bottom">
                    <p>${title}</p>
                    <h2>Price ${price}.00</h2>
                    <div class="button--like">
                     <button onClick="editedToCart(${k})">Edited to cart</button>
                    <button onClick="addAdminCart(${k++})">Deleted Cart</button>
                    </div>  
                </div>
            </div>`
    }).join("");
}
//*aynan aysi cartni o'chirib tashlashni belgilaydi
document.addEventListener("DOMContentLoaded", adminCartFunction());
const close=document.querySelector("#close");
const modal=document.querySelector(".wrapper__Modal");
const shadow=document.querySelector(".shadow");
const title=document.getElementsByClassName("input-title");
const price=document.getElementsByClassName("input-price");
const editedButton=document.querySelector("#modal__button");
function editedToCart(){
    modal.classList.toggle("remove");
    shadow.classList.toggle("remove");
}
close.addEventListener("click",()=>{
    modal.classList.toggle("remove");
    shadow.classList.toggle("remove");
})

function valueInput(index){
    categories[index].title=title.value;
    categories[index].price=price.value;
}


//*edit qilingan cartlarni sidebarga qo'shamiz
const editCart=[];
editedButton.addEventListener("click",(id)=>{
    valueInput(id);
   editCart.push(categories[id]);
    addEditedCart();
    modal.classList.toggle("remove");
    shadow.classList.toggle("remove");
})
function addEditedCart(){
    let l=0;
    document.querySelector(".likeItem").innerHTML=editCart.map(item=>{
        var { image, title, price, id } = item;
        return `<div class="box">
                <div className="image-box">
                    <img src=${image} alt="" />
                </div>
                <div className="bottom">
                    <p>${title}</p>
                    <h2>Price ${price}.00</h2>
                    <div class="button--like">
                     <button onClick="editedToCart(${l})">Edited to cart</button>
                    <button onClick="addAdminCart(${l++})">Deleted Cart</button>
                    </div>  
                </div>
            </div>`
    }).join("");
}